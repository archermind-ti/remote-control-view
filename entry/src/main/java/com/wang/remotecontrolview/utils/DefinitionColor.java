package com.wang.remotecontrolview.utils;

import ohos.agp.utils.Color;

import java.util.HashMap;
import java.util.Locale;

/**
 * @date 2021/4/19 15:45
 * @author zhouzide
 * @description TODO
 */

public class DefinitionColor extends Color {


   public DefinitionColor(String color) {
        this.parseColor(color);
   }

   private static final HashMap<String, Integer> sColorNameMap;
   static {
       sColorNameMap = new HashMap<>();
       sColorNameMap.put("black", 0xFF000000);
       sColorNameMap.put("darkgray", 0xFFA9A9A9);
       sColorNameMap.put("gray", 0xFF808080);
       sColorNameMap.put("lightgray", 0XFF778899);
       sColorNameMap.put("white", 0xFFFFFFFF);
       sColorNameMap.put("red", 0xFFFF0000);
       sColorNameMap.put("green", 0xFF008000);
       sColorNameMap.put("blue", 0xFF0000FF);
       sColorNameMap.put("yellow", 0xFFFFFF00);
       sColorNameMap.put("cyan", 0xFF00FFFF);
       sColorNameMap.put("magenta", 0xFFFF00FF);
       sColorNameMap.put("aqua", 0xFF00FFFF);
       sColorNameMap.put("fuchsia", 0xFFFF00FF);
       sColorNameMap.put("darkgrey", 0xFFA9A9A9);
       sColorNameMap.put("grey", 0xFF808080);
       sColorNameMap.put("lightgrey", 0xFFD3D3D3);
       sColorNameMap.put("lime", 0xFF00FF00);
       sColorNameMap.put("maroon", 0xFF800000);
       sColorNameMap.put("navy", 0xFF000080);
       sColorNameMap.put("olive", 0xFF808000);
       sColorNameMap.put("purple", 0xFF800080);
       sColorNameMap.put("silver", 0xFFC0C0C0);
       sColorNameMap.put("teal", 0xFF008080);
       sColorNameMap.put("dashed", 0x20FFFFFF);
   }
   public static int parseColor(String colorString) {
       if (colorString.charAt(0) == '#') {
           // Use a long to avoid rollovers on #ffXXXXXX
           long color = Long.parseLong(colorString.substring(1), 16);
           if (colorString.length() == 7) {
               color |= 0x00000000ff000000;
           } else if (colorString.length() != 9) {
               throw new IllegalArgumentException("Unknown color");
           }
           return (int)color;
       } else {
           Integer color = sColorNameMap.get(colorString.toLowerCase(Locale.ROOT));
           if (color != null) {
               return color;
           }
       }
       throw new IllegalArgumentException("Unknown color");
   }

}
