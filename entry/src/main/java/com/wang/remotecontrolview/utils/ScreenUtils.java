package com.wang.remotecontrolview.utils;

import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class ScreenUtils {

    public static int getScreenWidth(Context context){
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return attributes.width;
    }

    public static int getScreenHeight(Context context){
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return attributes.height;
    }
}
