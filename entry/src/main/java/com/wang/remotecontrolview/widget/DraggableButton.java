package com.wang.remotecontrolview.widget;

import com.wang.remotecontrolview.utils.DefinitionColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;

public class DraggableButton extends Button implements  Component.EstimateSizeListener,Component.DrawTask {

    private Paint mPaint;

    public DraggableButton(Context context) {
        this(context,null);
    }

    public DraggableButton(Context context, AttrSet attrSet) {
        this(context, attrSet,null);
    }

    public DraggableButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPaint = new Paint();
        mPaint.setStrokeWidth(1);

        // 设置测量组件的侦听器
        setEstimateSizeListener(this);
        addDrawTask(this);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        int radius = component.getWidth() / 2;
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(new DefinitionColor("#cdcdcd"));
        canvas.drawCircle(radius, radius , radius - 2, mPaint);
        // 按下时有背景变化
        if (isPressed()){
            mPaint.setStyle(Paint.Style.FILL_STYLE);
            mPaint.setColor(new DefinitionColor("#20000000"));
            canvas.drawCircle(radius, radius , radius - 4, mPaint);
        }

        /*if (text!=null&&!"".equals(text)){
            mPaint.setStyle(Paint.Style.FILL_STYLE);
            mPaint.setColor(Color.WHITE);
            mPaint.setTextSize(radius / 2);
            mPaint.getTextBounds(text);

            int textHeight = mRect.height;
            int textWidth = mRect.width;
            canvas.drawText(mPaint,text, radius - textWidth / 2, radius + textHeight / 2);
        }*/
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));
        return true;
    }

}
