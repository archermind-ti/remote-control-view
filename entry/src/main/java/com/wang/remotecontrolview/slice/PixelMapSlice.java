package com.wang.remotecontrolview.slice;

import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class PixelMapSlice {

    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "PixelMapSlice");

    private static byte[] readResource(Resource resource) {
        final int bufferSize = 1024;
        final int ioEnd = -1;
        byte[] byteArray;
        byte[] buffer = new byte[bufferSize];
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            while (true) {
                int readLen = resource.read(buffer, 0, bufferSize);
                if (readLen == ioEnd) {
                    HiLog.error(LABEL_LOG, "readResource finish");
                    byteArray = output.toByteArray();
                    break;
                }
                output.write(buffer, 0, readLen);
            }
        } catch (IOException e) {
            HiLog.debug(LABEL_LOG, "readResource failed " + e.getLocalizedMessage());
            return new byte[0];
        }
        HiLog.debug(LABEL_LOG, "readResource len: " + byteArray.length);
        return byteArray;
    }

    public static Optional<PixelMap> createPixelMapByResId(Context slice,int resourceId) {
        ResourceManager manager = slice.getResourceManager();
        if (manager == null) {
            return Optional.empty();
        }
        try (Resource resource = manager.getResource(resourceId)) {
            if (resource == null) {
                return Optional.empty();
            }
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(readResource(resource), srcOpts);
            if (imageSource == null) {
                return Optional.empty();
            }
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            decodingOpts.desiredSize = new Size(0,0);
            decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
            decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;

            return Optional.of(imageSource.createPixelmap(decodingOpts));
        } catch (NotExistException | IOException e) {
            return Optional.empty();
        }
    }

}
