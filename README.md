# RemoteControlViewHarmony

### 简介
本项目模仿乐视遥控App添加万能遥控器的交互效果

### 功能

遥控器交互组件


### 安装教程

方式一

1. 下载har包[RemoteControlView_ohos.har](https://gitee.com/archermind-ti/remote-control-view-ohos/releases/1.0.0)
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。    
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。


```
  dependencies {
     implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
     ……
  }
```

方式二

在project的build.gradle中添加mavenCentral()的引用

```
  repositories {   
 	...   
 	mavenCentral()   
	...           
  }
```

在entry的build.gradle中添加依赖

```
  dependencies { 
        ... 
        implementation 'com.gitee.archermind-ti:RemoteControlView_ohos:1.0.0' 
        ... 
  }
```
### 演示
<img src="./demo/demo.gif" width="20%" height="20%">

### 使用说明

1.在xml中直接使用，但是根布局需要是StackLayout

```
    <com.wang.remotecontrolview_ohos.RemoteControlView
        ohos:id="$+id:remote_control"
        ohos:height="480vp"
        ohos:width="match_parent"/>
```

2.在AbilitySlice中用PageSlider包含四组对应的按钮，分别对应四个Layout(Layout_pageSlider1,Layout_pageSlider2,Layout_pageSlider3,Layout_pageSlider4)

```
        List<Component> mPageViews = new ArrayList<>();

        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        TableLayout dependentLayout1 = (TableLayout) layoutScatter.parse(ResourceTable.Layout_pageSlider1, null, false);
        DirectionalLayout dependentLayout2 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_pageSlider2, null, false);
        TableLayout dependentLayout3 = (TableLayout) layoutScatter.parse(ResourceTable.Layout_pageSlider3, null, false);
        TableLayout dependentLayout4 = (TableLayout) layoutScatter.parse(ResourceTable.Layout_pageSlider4, null, false);

        mPageViews.add(dependentLayout1);
        mPageViews.add(dependentLayout2);
        mPageViews.add(dependentLayout3);
        mPageViews.add(dependentLayout4);
```
3.将PageView与PageSlider进行绑定

```
        mPageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return mPageViews.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(mPageViews.get(i));
                return mPageViews.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                (componentContainer).removeComponent(mPageViews.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
```
4.添加底部的滑动指示点

```
        RadioContainer mRadioContainer = (RadioContainer) findComponentById(ResourceTable.Id_radio_container);
        
        mPageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                ((RadioButton) mRadioContainer.getComponentAt(i)).setChecked(true);
            }
        });

```

### 编译说明
1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）

### 版本迭代
- v1.0   初始版本
[changelog](https://gitee.com/archermind-ti/remote-control-view-ohos/blob/master/changelog.md)

### 版权和许可信息

Copyright 2018 simplezhli

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
