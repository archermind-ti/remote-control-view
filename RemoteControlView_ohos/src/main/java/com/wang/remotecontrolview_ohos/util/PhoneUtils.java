package com.wang.remotecontrolview_ohos.util;

/**
 * 存储手机边界
 */
public class PhoneUtils {

    private int startX;
    private int endX;
    private int topY;
    private int bottomY;
    private static PhoneUtils newInstance;

    public static PhoneUtils getInstance(){
        if(newInstance == null){
            newInstance = new PhoneUtils();
        }
        return newInstance;
    }

    public void setData(int startX,int endX,int topY,int bottomY){
        this.startX = startX;
        this.endX = endX;
        this.topY = topY;
        this.bottomY = bottomY;
    }

    public int getStartX() {
        return startX;
    }

    public int getEndX() {
        return endX;
    }

    public int getTopY() {
        return topY;
    }

    public int getBottomY() {
        return bottomY;
    }
}
