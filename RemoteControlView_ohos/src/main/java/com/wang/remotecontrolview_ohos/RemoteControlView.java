package com.wang.remotecontrolview_ohos;

import com.wang.remotecontrolview_ohos.util.PhoneUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathEffect;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.Optional;

/**
 * 自定义遥控器View
 */

public class RemoteControlView extends Component implements Component.EstimateSizeListener, Component.DrawTask {

    private final static String TAG = "RemoteControlView";

    private final static HiLogLabel LABEL = new HiLogLabel(3, 0x00201, TAG);

    /**
     * 绘制手机画笔
     */
    private Paint mPhonePaint;

    /**
     * 手机返回按键path
     */
    private Path mBackPath;

    /**
     * 手机宽度
     */
    private int mPhoneWidth;

    /**
     * 手机内容区域高
     */
    private int mPhoneContentHeight;

    /**
     * 手机内容区域宽
     */
    private int mPhoneContentWidth;

    /**
     * 手机右上角x轴点
     */
    private int startX;

    private StackLayout frameLayout;
    private final static int WIDTH_COUNT = 4;
    private final static int HEIGHT_COUNT = 7;
    private final static float BORDER_COLOR_ALPHA = 0.44f;
    private final static float SOLID_COLOR_ALPHA = 0.17f;
    private final static float DASHED_COLOR_ALPHA = 0.13f;
    //虚线化
    private PathEffect mDashPathEffect = new PathEffect(new float[]{10, 10}, 0);


    public RemoteControlView(Context context) {
        this(context, null);
    }

    public RemoteControlView(Context context, AttrSet attrSet) {
        this(context, attrSet, 0);
    }

    public RemoteControlView(Context context, AttrSet attrSet, int defStyleAttr) {
        super(context, attrSet, defStyleAttr);

        initData(context);
        // 设置测量组件的侦听器
        setEstimateSizeListener(this);
    }


    private void initData(Context context) {
        initPaint();
        initContainers(context);
        addDrawTask(this);
    }

    private Paint circlePaint;

    private void initPaint() {
        mPhonePaint = new Paint();
        //抗锯齿
        mPhonePaint.setAntiAlias(true);
        mPhonePaint.setColor(Color.WHITE);
        mPhonePaint.setStyle(Paint.Style.STROKE_STYLE);
        mPhonePaint.setStrokeWidth(5);
        mPhonePaint.setTextSize(40);
        mPhonePaint.setTextAlign(TextAlignment.CENTER);

        mBackPath = new Path();
    }

    private void initContainers(Context context) {
        // 拖拽有效区域
        frameLayout = new StackLayout(context);
        ShapeElement element = new ShapeElement();
        //element.setRgbColor(new RgbColor(1, 155, 227));
        frameLayout.setBackground(element);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = EstimateSpec.getSize(widthEstimateConfig);
        int height = EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                EstimateSpec.getChildSizeWithMode(width, width, EstimateSpec.NOT_EXCEED),
                EstimateSpec.getChildSizeWithMode(height, height, EstimateSpec.NOT_EXCEED));
        // 手机高度为View高度减去上下间隔24dp
        int phoneHeight = height - dp2px(24);
        // 手机内容区域高 ：手机高度 - 手机头尾（48dp）- 手机屏幕间距（5dp） * 2）
        mPhoneContentHeight = phoneHeight - dp2px(58);
        // 手机内容区域宽 ：手机内容区域高/ 7 * 4（手机内容区域为4：7）
        mPhoneContentWidth = mPhoneContentHeight / HEIGHT_COUNT * WIDTH_COUNT;
        // 手机宽度为手机内容区域宽 + 手机屏幕间距 * 2
        mPhoneWidth = mPhoneContentWidth + dp2px(10);
        // 绘制起始点
        startX = (width - mPhoneWidth) / 2;
        return true;
    }

    private RectFloat mRectF = new RectFloat();

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int i = dp2px(12);
        // 绘制手机外壳
        mRectF.left = startX;
        mRectF.right = component.getWidth() - startX;
        mRectF.top = i;
        mRectF.bottom = component.getHeight() - i;
        canvas.drawRoundRect(mRectF, i, i, mPhonePaint);
        // 绘制手机上下两条线
        canvas.drawLine(startX, i * 3, component.getWidth() - startX, i * 3, mPhonePaint);
        canvas.drawLine(startX, component.getHeight() - i * 3, component.getWidth() - startX, component.getHeight() - i * 3, mPhonePaint);

        // 绘制手机上方听筒、摄像头
        mRectF.left = component.getWidth() / 2 - dp2px(25);
        mRectF.right = component.getWidth() / 2 + dp2px(25);
        mRectF.top = dp2px(22);
        mRectF.bottom = dp2px(26);
        canvas.drawRoundRect(mRectF, dp2px(2), dp2px(2), mPhonePaint);
        canvas.drawCircle(component.getWidth() / 2 - dp2px(40), i * 2, i / 3, mPhonePaint);
        canvas.drawCircle(component.getWidth() / 2 + dp2px(40), i * 2, i / 3, mPhonePaint);
        // 绘制手机下方按键
        canvas.drawCircle(component.getWidth() / 2, component.getHeight() - i * 2, i / 2, mPhonePaint);
        canvas.drawRect(startX + mPhoneWidth / 5, component.getHeight() - dp2px(29), startX + mPhoneWidth / 5 + dp2px(10), component.getHeight() - dp2px(19), mPhonePaint);
        mBackPath.moveTo(component.getWidth() - startX - mPhoneWidth / 5, component.getHeight() - dp2px(30));
        mBackPath.lineTo(component.getWidth() - startX - mPhoneWidth / 5 - dp2px(10), component.getHeight() - dp2px(24));
        mBackPath.lineTo(component.getWidth() - startX - mPhoneWidth / 5, component.getHeight() - dp2px(18));
        mBackPath.close();
        canvas.drawPath(mBackPath, mPhonePaint);

        // 绘制网格（4 * 7的田字格）田字格外框为实线，内侧为虚线
        // 手机屏幕间距5pd
        int j = dp2px(5);
        // 格子的宽高
        int size = mPhoneContentHeight / HEIGHT_COUNT;
        HiLog.warn(LABEL, "width is %{public}s", size);
        // 横线
        for (int z = 0; z <= HEIGHT_COUNT; z++) {
            mPhonePaint.setPathEffect(null);
            mPhonePaint.setAlpha(SOLID_COLOR_ALPHA);
            mPhonePaint.setColor(Color.WHITE);
            mPhonePaint.setStrokeWidth(1);
            // 实线
            canvas.drawLine(startX + j, dp2px(41) + z * size,
                    component.getWidth() - startX - j, dp2px(41) + z * size, mPhonePaint);
            // 虚线
            if (z != HEIGHT_COUNT) {
                mPhonePaint.setPathEffect(mDashPathEffect);
                mPhonePaint.setAlpha(DASHED_COLOR_ALPHA);
                mPhonePaint.setColor(Color.WHITE);
                canvas.drawLine(startX + j, dp2px(41) + z * size + size / 2,
                        component.getWidth() - startX - j, dp2px(41) + z * size + size / 2, mPhonePaint);
            }
        }

        // 竖线
        for (int z = 0; z <= WIDTH_COUNT; z++) {
            mPhonePaint.setPathEffect(null);
            mPhonePaint.setAlpha(SOLID_COLOR_ALPHA);
            mPhonePaint.setColor(Color.WHITE);
            mPhonePaint.setStrokeWidth(1);
            // 实线
            canvas.drawLine(startX + j + z * size, dp2px(41),
                    startX + j + z * size, component.getHeight() - dp2px(41), mPhonePaint);
            // 虚线
            if (z != WIDTH_COUNT) {
                mPhonePaint.setPathEffect(mDashPathEffect);
                mPhonePaint.setAlpha(DASHED_COLOR_ALPHA);
                mPhonePaint.setColor(Color.WHITE);
                canvas.drawLine(startX + j + z * size + size / 2, dp2px(41),
                        startX + j + z * size + size / 2, component.getHeight() - dp2px(41), mPhonePaint);
            }
        }
        PhoneUtils.getInstance().setData(startX + dp2px(5), component.getWidth() - startX - dp2px(5), dp2px(41), component.getHeight() - dp2px(41));
    }

    private int dp2px(int dp) {
        Optional<Display> defaultDisplay = DisplayManager.getInstance().getDefaultDisplay(getContext());
        DisplayAttributes attributes = defaultDisplay.get().getAttributes();
        float density = attributes.scalDensity;
        return (int) (dp * density + 0.5f);
    }
}
